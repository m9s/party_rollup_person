# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"Party Person Rollup"
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Equal, Eval, Not, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool


class Party(ModelSQL, ModelView):
    _name = "party.party"

    person_members = fields.One2Many('party.rollup.organization-person',
            'organization', 'Person Members', states={
                    'readonly': Not(Bool(Eval('active'))),
                    'invisible': Equal(Eval('party_type'), 'person'),
            }, depends=[
                'party_type', 'active',
            ], help='Persons which are involved in this organization.')
    organization_members = fields.One2Many('party.rollup.organization-person',
            'person', 'Member of Organizations', states={
                'readonly': Not(Bool(Eval('active'))),
                'invisible': Equal(Eval('party_type'), 'organization'),
            }, depends=[
                'party_type', 'active',
            ], help='Organizations where this person is member.')

Party()


class PartyRollupOrganizationPerson(ModelSQL, ModelView):
    '''Person in Organization'''
    _name = 'party.rollup.organization-person'
    _rec_name = 'person'
    _description = __doc__

    person = fields.Many2One('party.party', 'Person', required=True, domain=[
                    ('party_type', '=', 'person'),
            ], context={'party_type': 'person'}, ondelete='CASCADE')
    organization = fields.Many2One('party.party', 'Organization',
            required=True, domain=[
                    ('party_type', '=', 'organization'),
            ], ondelete='CASCADE')

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        party_obj = Pool().get('party.party')
        for record in self.browse(ids):
            party_rec_name = party_obj.browse(record.person.id).rec_name
            res[record.id] = party_rec_name
        return res

PartyRollupOrganizationPerson()
